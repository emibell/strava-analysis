import numpy as np
import glob, os
from datetime import datetime
import calendar

#=================
def add_zero(t):
    t = str(int(t))
    if len(t) < 2:
        t = '0'+t
    return t

#=================
def hr_dec_to_str(hh):
    h = np.floor(hh)
    if h != 0:
        m = np.floor((hh%h)*60.)
        s = np.floor((((hh%h)*60.)%m)*60.)
    else:
        m = np.floor(hh*60.)
        s = np.floor(((hh*60.)%m)*60.)
    if np.isnan(s):
        s = 0
    time_str = ':'.join([add_zero(h),add_zero(m),add_zero(s)])
    if h >= 24:
        d = np.floor(h/24.)
        h = np.floor(h%24)
        time_str = ':'.join([add_zero(d),add_zero(h),add_zero(m),add_zero(s)])
    return time_str

#=================
def sum_time(hhmmss):
    h_total = 0
    m_total = 0
    s_total = 0
    for i in range(0,len(hhmmss)):
        #breakpoint()
        while len(hhmmss[i].split(':')) < 3:
            hhmmss[i] = '00:'+hhmmss[i]
        h,m,s = hhmmss[i].split(':') 
        h_total+=int(h)
        m_total+=int(m)
        s_total+=int(s)
        
    # convert seconds to minutes
    m_total+=np.floor(s_total/60.)
    s_total=s_total%60.
    # convert minutes to hours
    h_total+=np.floor(m_total/60.)
    m_total=m_total%60.
    # construct totals
    total_str = '{}h{}m{}s'.format(int(h_total),int(m_total),int(s_total))
    total_str_2 = ':'.join([add_zero(h_total),add_zero(m_total),add_zero(s_total)])
    total_dec = h_total+m_total/60.+s_total/3600.
    # add days
    if h_total >= 24:
        d_total=np.floor(h_total/24.)
        h_total=h_total%60.
        total_str = '{}d{}h{}m{}s'.format(int(d_total),int(h_total),int(m_total),int(s_total))
        total_str_2 = ':'.join([add_zero(d_total),add_zero(h_total),add_zero(m_total),add_zero(s_total)])

    return [total_str_2,total_dec]

#=================
def get_cal(dt):
    # input: datetime object 
#    output: array with weekday header and day numbers, along with nweeks
    calendar.setfirstweekday(6) #make weeks start on sunday
    weekheader = calendar.weekheader(3).split(' ')
    days = calendar.monthcalendar(dt.year,dt.month)
    weeks = np.linspace(1,len(days),len(days))
    return [weekheader,days,weeks]

#=================
def aggregate_per_day(month,cal_days,dt,var,time=False):
    dt_months = [d.month for d in dt] 
    month_mask = [m == month for m in dt_months]
    dt_days = [d.day for d in dt] 
    var_per_day = []
    var_per_day_string = []
    for week in cal_days:
        var_week = []
        var_week_string = []
        for day in week:
            day_mask = [d == day for d in np.array(dt_days)[month_mask]]
            if np.sum(day_mask) == 0:
                var_week.append(np.nan)
                if day == 0:
                    var_week_string.append('')
                else:
                    # add date to string (1-31)
                    var_week_string.append(str(int(day)))
            else:
                if time is True:
                    daily_val = sum_time(var[month_mask][day_mask])
                    var_week.append(daily_val[1])
                    var_week_string.append(str(int(day))+'\n\n'+daily_val[0])
                else:
                    daily_val = np.sum(var[month_mask][day_mask])
                    var_week.append(daily_val)
                    var_week_string.append(str(int(day))+'\n\n'+daily_val)
        var_per_day.append(var_week)
        var_per_day_string.append(var_week_string)

    return [var_per_day,var_per_day_string]
    
