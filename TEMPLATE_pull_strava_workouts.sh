#!/bin/bash

for pagenumber in {1..<4>}
do
    HTTP_CALL=$(<cURL> > <output_file>_$pagenumber.json)
    echo $HTTP_CALL
done

exit 0
