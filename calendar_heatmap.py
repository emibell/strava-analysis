# !/usr/bin/python
# encoding: utf-8
import numpy as np
import pandas as pd
import glob, os, json, re, argparse
import pandas as pd
from importlib import reload
from matplotlib import pyplot as plt 
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from plot_workouts import *
from misc_tools import *
from data_tools import *
from datetime import datetime
import calendar

def strava_workout_types():
    types = [
        'Run','Trail Run','Walk','Hike',
        'Ride','Mountain Bike Ride','Gravel Ride',
        'E-Bike Ride','E-Mountain Bike Ride','Velomobile Ride',
        'Swim','Canoe','Kayak','Kitesurf Session',
        'Row','Stand Up Paddle','Surf','Windsurf Session',
        'Sail','Alpine Ski','Backcountry Ski','Ice Skate',
        'Nordic Ski','Snowboard','Snowshoe',
        'Crossfit','Elliptical','Handcycle','Inline Skate',
        'Rock Climb','Roller Ski','Stair Steppper','Weight Training',
        'Wheelchair','Workout','Yoga','Golf',
        'Football (Soccer)','Skateboard']
    types = [t.replace(' ','') for t in types]
    return types

def month_year_suffix(ti,sn):
    suffix = [',']
    if args.month:
        ms = pd.to_datetime(data['start_time'].iloc[0]).strftime('%B')
        suffix.append(ms)
        sn = sn.replace('.png','_'+ms+'.png')
    if args.year:
        ys = pd.to_datetime(data['start_time'].iloc[0]).strftime('%Y')
        suffix.append(ys)
        sn = sn.replace('.png','_'+ys+'.png')
    if args.month or args.year:
        ti+=' '.join(suffix)
    return ti,sn

def monthly_heatmap():
    dt_this = datetime(day=1,month=args.month,year=args.year)
    cal_header,cal_days,cal_weeks = get_cal(dt_this)
    dt = [datetime.fromtimestamp(d) for d in data['start_date_local_raw']]
    # maybe adjust so aggregate_per_day 
    # doesn't need the calendar matrix.
    # that way we could calc per-day for the whole
    # record to automate a time_lim.
    # assigning times to the calendar matrix
    # could be a second step.
    time_per_day,time_per_day_string = aggregate_per_day(
        args.month,
        cal_days,
        dt,
        data['elapsed_time'].to_numpy(),
        time=True
    )
    title='Active Minutes Per Day, ' \
        + calendar.month_name[args.month] \
        + ' '+str(int(args.year))
    sn = (output_savedir
        + calendar.month_name[args.month]+'_'
        + str(int(args.year))+'_'
        + 'time'+'_'
        + 'heatmap.png'
    )
    if args.workout_type != 'all':
        title = title.replace(
            'Active Minutes',
            'Active '+args.workout_type+' Minutes'
        ) 
        sn = sn.replace(
            'time',
            'time_'+args.workout_type
        )
    heat_map_calendar(
        time_per_day,
        xticks=cal_header,
        color=args.colormap,
        labels=time_per_day_string,
        title=title,savename=sn
    )


if __name__ == "__main__":
    """
    strava_types_pie_chart.py
    Generate a monthly heat map of pre-defined Strava workout types.
    Default behavior is to generate a heat map for the current month. 
    Use command line arguments to specify a month
    and/or year.
    Default colormap is "OrangePink", but can be changed 
    to any matplotlib standard or strava-analysis custom colormap
    using the colormap command line argument (-c).
    """
    today = datetime.today()

    # define command line arguments
    parser = argparse.ArgumentParser(
        prog = 'Active Minutes Heat Map',
        description = 'Produces heat map of active minutes per day for given month. Defaults to sum of all workout types and current month.',
        epilog = 'Contact theembell@gmail.com with further questions or bug reports.')

    parser.add_argument(
        '-m', '--month',default=today.month,type=int,
        help='Required: Month for heat map. Defaults to current month.',
        required=True)

    parser.add_argument(
        '-y','--year',default=today.year,type=int,
        help='Required: Year for heat map. Defaults to current year.',
        required=True)

    parser.add_argument(
        '-c','--colormap',default='OrangePink',
        help='Optional: Color scheme for non-heatmap visualizations. Default is "OrangePink."')

    parser.add_argument(
        '-w','--workout_type',default='all',
        help='Optional: Strava-defined workout type to plot as heat map.')

    args = parser.parse_args()

    time_max = 3.5  # colorbar max, in hours of activity
    json_file = './strava_json_files/all_workouts.json'
    data = pd.read_json(json_file)
    output_savedir='./figures/'

    # workout type keywords
    type_keywords = strava_workout_types()

    # month/year mask if specified
    # and workout type as well
    dt = pd.to_datetime(data['start_time']) #convert to datetime objects
    mask = [True]*len(data)
    if args.month:
        mask_m = [int(args.month) == d.month for d in dt] 
        mask = [all(ww) for ww in zip(mask,mask_m)]
    if args.year:
        mask_y = [int(args.year) == d.year for d in dt]
        mask = [all(ww) for ww in zip(mask,mask_y)]
    if args.workout_type != 'all':
        mask_wt = [args.workout_type.lower() in d.lower() for d in data['type']]
        mask = [all(ww) for ww in zip(mask,mask_wt)]
    data = data[mask]

    monthly_heatmap()

