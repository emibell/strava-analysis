# !/usr/bin/python
# encoding: utf-8
import numpy as np
import pandas as pd
import glob, os, json, re, argparse
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from plot_workouts import *
from misc_tools import *
from data_tools import *
from datetime import datetime
import calendar

#=================
def get_stats(data,kws,search_fields):
    """ Gets a mask and a count for each of the 
        specified keywords within the 
        data DataFrame.
        
        Inputs:
            data                PandasDataFrame of Strava workouts 
            kws                 list of keywords to search for in specified field 
            search_fields       fields within data in which to search for kws    
    """
    if type(search_fields) is not list:
        search_fields = [search_fields]
    stats = pd.DataFrame(columns=[
        'keyword','mask','count',
        'total_time_decimal','total_time_string'])
    stats['keyword'] = kws
    for i,k in enumerate(kws):
        mask = [False]*len(data)
        for f in search_fields:
            mask_f = [
                (n is not None) and (k.lower() in n.lower()) \
                for n in data[f]
            ]
            mask = [any(ww) for ww in zip(mask,mask_f)]
        stats['mask'].iloc[i] = mask
        stats['count'].iloc[i] = np.sum(stats['mask'].iloc[i])
        t,s = sum_time(data[mask]['elapsed_time'].to_numpy())
        stats['total_time_string'].iloc[i] = t
        stats['total_time_decimal'].iloc[i] = s
    return stats

#=================
def strava_workout_types():
    types = [ 
        'Run','Trail Run','Walk','Hike',
        'Ride','Mountain Bike Ride','Gravel Ride',
        'E-Bike Ride','E-Mountain Bike Ride','Velomobile Ride',
        'Swim','Canoe','Kayak','Kitesurf Session',
        'Row','Stand Up Paddle','Surf','Windsurf Session',
        'Sail','Alpine Ski','Backcountry Ski','Ice Skate',
        'Nordic Ski','Snowboard','Snowshoe',
        'Crossfit','Elliptical','Handcycle','Inline Skate',
        'Rock Climb','Roller Ski','Stair Steppper','Weight Training',
        'Wheelchair','Workout','Yoga','Golf',
        'Football (Soccer)','Skateboard'
    ]
    types = [t.replace(' ','') for t in types]
    return types
