import pandas as pd
import glob, os, json
import pandas as pd

json_dir = './strava_json_files/'
combined_file_name = json_dir+'all_workouts.json'


json_files = glob.glob(json_dir+'workouts_*.json')
jst = open(json_files[0])
jsone = json.load(jst)
js_keys = jsone['models'][0].keys()
jst.close()
data = pd.DataFrame(columns=js_keys)

i = 0
print('Importing Strava JSON data...')
for js in json_files:
    js_open = open(js)
    js_dat = json.load(js_open)
    js_open.close()

    #breakpoint()
    for m in js_dat['models']:
        datm = []
        for k in data.keys():
           datm.append(m[k]) 
        data.loc[i] = datm
        #js_date = m['start_date'].split(', ')[-1]
        #js_name = m['name']
        #js_type = m['type']
        #js_duration = m['elapsed_time']
        #js_note = m['description']
        #data.loc[i] = [js_date,js_name,js_type,js_duration,js_note]
        #breakpoint()
        i+=1
    
data['datetime'] = pd.to_datetime(data['start_date'])
data = data.sort_values(by='datetime',ascending=True)

print('Saving to '+combined_file_name)
data.to_json(combined_file_name,orient='records')
print('Done.')
