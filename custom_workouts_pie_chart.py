# !/usr/bin/python
# encoding: utf-8
import numpy as np
import pandas as pd
import glob, os, json, re, argparse
import pandas as pd
from importlib import reload
from matplotlib import pyplot as plt 
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import plot_workouts as pw
import misc_tools as misct
import data_tools as dtools
from datetime import datetime
import calendar

def month_year_suffix(ti,sn):
    suffix = [',']
    if args.month:
        ms = pd.to_datetime(data['start_time'].iloc[0]).strftime('%B')
        suffix.append(ms)
        sn = sn.replace('.png','_'+ms+'.png')
    if args.year:
        ys = pd.to_datetime(data['start_time'].iloc[0]).strftime('%Y')
        suffix.append(ys)
        sn = sn.replace('.png','_'+ys+'.png')
    if args.month or args.year:
        ti+=' '.join(suffix)
    return ti,sn

def pie_chart_counts():
    # figure title and file save name
    type_title = 'Instances of User-Defined Workout Types'
    sn = output_savedir+'user_workout_types_pie.png'
    # generate month and year suffixes if specified
    type_title,sn = month_year_suffix(type_title,sn) 
    # type_header will sit at the top of the legend
    type_header = (
        'N = '
        + str(int(sum(stats['count'].to_numpy())))
        + ' total entries'
    )
    pw.pie_chart(
        stats['count'].to_numpy(),
        type_keywords,
        color=args.colormap,
        ncount=True,
        title=type_title,
        header=type_header,
        savename=sn
    )
    print('Saved as '+sn)
    return

def pie_chart_times():
    type_times_dec = stats['total_time_decimal'].to_numpy()
    type_times_header = misct.hr_dec_to_str(sum(type_times_dec))+' total time'
    type_times_title = 'Active Minutes of User-Defined Workout Types'
    sn = output_savedir+'user_workout_types_times_pie.png'
    type_times_title,sn = month_year_suffix(type_times_title,sn)
    pw.pie_chart(
        type_times_dec,
        type_keywords,
        color=args.colormap,
        ncount=True,
        header=type_times_header,
        title=type_times_title,
        time=True,
        savename=sn
    )
    print('Saved as '+sn)
    return

if __name__ == "__main__":
    """
    strava_types_pie_chart.py
    Generate a pie chart of pre-defined Strava workout types.
    Default behavior is to generate a pie chart for the full
    workout record. Use command line arguments to specify a month
    and/or year.
    Default colormap is multi_bright, but can be changed using
    the colormap command line argument (-c).
    """

    # define command line arguments
    parser = argparse.ArgumentParser(
        prog = 'Strava Workout Types Pie Chart',
        description = 'Produces pie chart workout types. Defaults to pre-defined Strave workout types over the full record.',
        epilog = 'Contact theembell@gmail.com with further questions or bug reports.')

    parser.add_argument(
        '-m', '--month',default=None,
        help='Month for pie chart (integer).')

    parser.add_argument(
        '-y','--year',default=None,
        help='Year for pie chart (YYYY).')

    parser.add_argument(
        '-c','--colormap',default='autumnal',
        help='Color scheme for non-heatmap visualizations. Default is "autumnal."')

    args = parser.parse_args()    

    json_file = './strava_json_files/all_workouts.json'
    data = pd.read_json(json_file)
    output_savedir='./figures/'

    # workout type keywords
    cfile = open('./custom_search_terms.csv','r')
    type_keywords = pd.read_csv(cfile).keys().to_numpy()

    # month/year mask if specified
    dt = pd.to_datetime(data['start_time']) #convert to datetime objects
    mask = [True]*len(data)
    if args.month:
        mask_m = [int(args.month) == d.month for d in dt] 
        mask = [all(ww) for ww in zip(mask,mask_m)]
    if args.year:
        mask_y = [int(args.year) == d.year for d in dt]
        mask = [all(ww) for ww in zip(mask,mask_y)]
    data = data[mask]

    # get workout mask, count, total time
    stats = dtools.get_stats(
        data,
        type_keywords,
        search_fields=['type','description','name']
    )

    # first figure: number of workouts
    pie_chart_counts()

    # second figure: total time doing workouts
    pie_chart_times()


