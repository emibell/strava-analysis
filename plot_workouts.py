import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import glob, os, re
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from misc_tools import *
from seaborn import heatmap

#=================
def mycmaps(name):
    # these colormaps were developed using tools at 
    # https://coolors.co/
    if name not in plt.colormaps():
        if name == 'multi_bright':
            colorlist = ["97c125","20a39e","23001e","d82f5a","F5853F"] 
            #colorlist = ["97c125","20a39e","d82f5a","F5853F"] 
            #colorlist = ["97c125","20a39e","8f0079","d82f5a","f4772a"]
            #colorlist = ["97c125","20a39e","8f0079","d82f5a","F5A72A"]
            #colorlist = ["97c125","218DA3","8f0079","d82f5a","F5A72A"]
        elif name == 'multi_muted':
            #colorlist = ["2a9d8f","264653","e76f51","f4a261","e9c46a"] 
            colorlist = ["d97171","f8bf63","b1c774","7ebbc9","AA8CC9"]  
        elif name == 'returns':
            colorlist = ["dacc3e","bd461a","781c17","385b9b","8bd0a1"]
        elif name == 'OrangePink':
            #colorlist = ["fccdb1","E6534E","4c0038"]
            #colorlist = ["fccdb1","dc3f54","400530"]
            #colorlist = ["ffd2b8","ffaf84","dc3f54","89043d","400530"]
            colorlist = ["fff2eb","F58E6C","801c48","3e002e"] 
        elif name == 'pinkie':
            colorlist = ["F8F1F6","e394b3","f57696","d82f5a","450d1b"]
        elif name == 'orange':
            colorlist = ["fef3ec","f5853f","271002"]
            #colorlist = ["E7D7C2","db995a","d36821","c93d0f","511a07"]
        elif name == 'blue':
            colorlist = ["ddf8f7","20a39e","072221"]
        elif name == 'green':
            colorlist = ["f8fcee","97c125","1b2306"]
        elif name == 'GreenBlue':
            #colorlist = ['F0F3CE',"9abf8f","36494e","111718"]
            colorlist = ["dfefd1","54b67c","2d7575","214952","001021"]
        elif name == 'calm':
            #colorlist = ["E2DCD1","b5b192","918b76","626c66","434a42"] 
            colorlist = ["e2dcd1","b5b192","8a9176","626c66","344a45"]
        elif name == 'bright_sunset':
            colorlist = ["390099","9e0059","ff0054","ff5400","ffbd00"]
        elif name == 'autumnal':
            #colorlist = ["0f4c5c","5f0f40","9a031e","fb8b24","e36414"]
            colorlist = ["597818","0f4c5c","5f0f40","9a031e","e36414"]

        colorlist = ['#'+l for l in colorlist]
        cmap = LinearSegmentedColormap.from_list('test_cmap',colors=colorlist,N=256)
    else:
        try:
            cmap = plt.cm.get_cmap(name,256)
        except:
            print('cmap name not recognized. defaulting to viridis.')
            cmap = plt.cm.get_cmap('viridis',256)

    return cmap
         

#=================
def pie_chart(counts,labels,ncount=None,header=None,title=None,time=None,savename=None,topN=6,color='multi_bright'):
    # ncount=True: include "N=NN" in labels. full label is "N=NN (MM%)". otherwise label is MM%
    # header=string: header line for the legend
    # title=string: figure title in strava orange
    # time=True: converts values to time strings dd:hh:mm:ss for legend
    # savename=string: full path for saving png at 500 dpi
    # topN=int(): number of categories to include in the pie chart.
    #   you can imagine 200 different slices might be overwhelming. default topN=6.
    # *position_pie_labels function is not yet functional.
    #   the idea is to auto-space labels if the slices are so thin they'll overlap.
    def position_pie_labels():
        for j,p in enumerate(count_pcts):
            if p < 10:
                texts[j].set_labeldistance()

    # initialize colorbar
    cmap = mycmaps(color)
    colors = cmap(np.linspace(0,1,topN))

    # remove any 0 counts - we don't want those visualized
    labels = np.array(labels)[counts != 0]
    counts = counts[counts != 0]
    # sort in order of size, largest to smallest
    ind = np.argsort(np.array(counts))[::-1]
    # if there are more than 6 categories, let's only plot the first 6
    if len(ind) > topN:
        ind = ind[0:topN]
    counts = np.array(counts)[ind]
    labels = np.array(labels)[ind]

    fig,ax = plt.subplots(figsize=[7,5])
    for i,l in enumerate(labels):
        if l.isupper() is False:
            labels[i] = re.sub(r"(?<=\w)([A-Z])", r" \1",l)
    #^ insert a space if there are two capitalized words
    count_pcts = [(c/sum(counts))*100. for c in counts]
    count_pcts = ['{:.2f}%'.format(cc) for cc in count_pcts] # start with percentage
    if ncount is not None:
        if time is not None:
            count_labels = [hr_dec_to_str(c)+' (' for c in counts]
        else:
            # add the count first, percentage in parentheses afterwards.
            count_labels = ['N = {:.0f} ('.format(c) for c in counts]
        count_labels = [c+cc+')' for c,cc in zip(count_labels,count_pcts)]


    patches,texts = ax.pie(counts,colors=colors,labels=labels)
    for i,t in enumerate(texts):
        t.set_color(colors[i])
        t.set_fontsize(13.5)
    if title is not None:
        ax.set_title(title,weight='bold',color='#F24405')
        ax.title.set_fontsize(15)

    pie_chart_legend(count_labels,counts,colors,header=header)

    if savename is not None:
        plt.figure(1).savefig(savename,dpi=500,bbox_inches='tight')
    plt.close('all')

    return

#=================
def pie_chart_legend(labels,counts,colors,header=None):
    font_max = 11
    font_min = 8
    smallest = float(font_min)/float(font_max)
    mins = np.linspace(1,smallest,len(counts))
    size = font_max*mins
    pos = [-2.1,0.765]
    vspace = 0.11
    if header is not None:
        plt.text(pos[0],pos[1]+1*vspace,header,fontsize=12,weight='bold',color='#A8A8A7')
    for i,l in enumerate(labels):
        plt.text(pos[0],pos[1]-(i*vspace),l,color=colors[i],fontsize=size[i])
    #breakpoint()
    return

#=================
def heat_map_calendar(vals,xticks='auto',yticks='auto',labels=None,savename=None,title=None,note=None,vmin=0,vmax=3.5,color='OrangePink'):
    cmap = mycmaps(color)
    annot_kws = {'fontsize':8,'verticalalignment':'top'}
    hm = heatmap(vals,cmap=cmap,annot=labels,fmt='s',square=True, \
        xticklabels=xticks,annot_kws=annot_kws, \
        linewidths=0.5,linecolor='#eeebeb', \
        vmin=vmin,vmax=vmax,cbar=False)
    hm.xaxis.tick_top()
    hm.xaxis.set_ticks_position('none')
    hm.axes.get_yaxis().set_visible(False)
    if title is not None:
        plt.title(title,pad=15,fontsize=13,weight='bold')
    if savename is not None:
        plt.figure(1).savefig(savename,dpi=500,bbox_inches='tight')
    plt.close('all')
    return


#=================
#=================
plt.rcParams['font.family']='Arial' #currently set as a global variable
